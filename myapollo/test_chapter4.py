# -*- coding: utf-8 -*-
import pytest


@pytest.fixture(scope='module', params=['start', 'stop'])
def action(request):
    print(f"request = {request}")
    print(f"request.param = {request.param}")
    return request.param


def test_action(action):
    print("test_action")
    assert 'pause' == action
