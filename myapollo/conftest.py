# -*- coding: utf-8 -*-
import pytest


class DB(object):
    def __init__(self):
        print("DB_INIT")
        print(id(self))


@pytest.fixture(scope="module")
def db():
    return DB()
