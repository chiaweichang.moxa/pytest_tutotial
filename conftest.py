# from test_foocompare import Foo


# def pytest_assertrepr_compare(op, left, right):
#     if isinstance(left, Foo) and isinstance(right, Foo) and op == "==":
#         return ["Comparing Foo instances:", "   vals: %s != %s" % (left.val, right.val)]


# content of conftest.py

# import pytest
# import smtplib


# @pytest.fixture(scope="function")
# def smtp_connection():
#     return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)


# content of conftest.py

# import smtplib
# import pytest


# @pytest.fixture(scope="module")
# def smtp_connection():
#     smtp_connection = smtplib.SMTP("smtp.gmail.com", 587, timeout=5)
#     yield smtp_connection  # provide the fixture value
#     print("teardown smtp")
#     smtp_connection.close()


# import smtplib
# import pytest


# @pytest.fixture(scope="module")
# def smtp_connection():
#     with smtplib.SMTP("smtp.gmail.com", 587, timeout=5) as smtp_connection:
#         yield smtp_connection  # provide the fixture value


import pytest
import smtplib


@pytest.fixture(scope="module")
def smtp_connection(request):
    server = getattr(request.module, "smtpserver", "smtp.gmail.com")
    smtp_connection = smtplib.SMTP(server, 587, timeout=5)
    yield smtp_connection
    print("finalizing %s (%s)" % (smtp_connection, server))
    smtp_connection.close()
