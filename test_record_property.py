# content of test_example.py
import pytest


@pytest.fixture(scope="session", autouse=True)
def log_global_env_facts(record_testsuite_property):
    record_testsuite_property("ARCH", "PPC")
    record_testsuite_property("STORAGE_TYPE", "CEPH")


class TestMe(object):
    def test_foo(self):
        assert True


def test_ok(
    record_xml_attribute,
    record_property
):
    record_property("example_key", 1)
    record_xml_attribute("assertions", "REQ-1234")
    record_xml_attribute("classname", "custom_classname")
    print("ok")


def test_fail():
    assert 0
