import pytest


def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        1 / 0


@pytest.mark.xfail
def test_recursion_depth():
    with pytest.raises(RuntimeError) as excinfo:
        def f():
            f()
        f()
    print(excinfo.value)
    assert "maximum recursion" in str(excinfo.value)
    assert 1 == 2


def myfunc():
    raise ValueError("Exception 123 raised")


def test_match():
    with pytest.raises(ValueError, match=r".* 123 .*"):
        myfunc()


@pytest.mark.xfail(raises=IndexError)
def test_f():
    max([1, 2, 3, 4, 6, 7, 8, 9, 0])

# pytest -rA test_expect_error.py


def test_eq_long_text():
    a = ["1"] * 5 + ["a"] + ["2"] * 5
    b = ["1"] * 5 + ["b"] + ["2"] * 5
    assert a == b
